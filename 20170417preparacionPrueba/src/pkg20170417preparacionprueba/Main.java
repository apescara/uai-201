/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170417preparacionprueba;
import java.util.Scanner;
/**
 *
 * @author andre
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        bancoUAI banco = new bancoUAI();
        Scanner scan = new Scanner(System.in);
        String RUN;
        String nombre;
        String apellido;
        int fono;
        String direccion;
        int idCliente;
        int idCuenta;
        double monto;
        System.out.println("Ingrese nombre:");
        nombre = scan.nextLine();
        System.out.println("Ingrese apelido");
        apellido = scan.nextLine();
        System.out.println("Ingrese RUN");
        RUN = scan.nextLine();
        System.out.println("Ingrese direccion");
        direccion = scan.nextLine();
        System.out.println("Ingrese telefono");
        fono = scan.nextInt();
        banco.crearCliente(RUN, nombre, apellido, direccion, fono);
        idCliente = banco.buscaraCliente(nombre, apellido);
        banco.crearCC(idCliente);
        idCuenta = banco.buscaCuenta(idCliente);
        System.out.println("Ingrese monto a depositar");
        monto = scan.nextInt();
        banco.cuentas.get(idCuenta).agregarSaldo(monto);
        banco.revisionMensual();
        monto = banco.cuentas.get(idCuenta).getSaldo();
        System.out.println("la persona " + nombre + " " + apellido +
                " luego de la revision mensual tiene un saldo de: " + monto);
        
        
    }
    
}
