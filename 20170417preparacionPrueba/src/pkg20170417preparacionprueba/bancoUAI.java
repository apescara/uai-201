/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170417preparacionprueba;
import java.util.ArrayList;
/**
 *
 * @author andre
 */
public class bancoUAI {
    final ArrayList<Cuenta> cuentas = new ArrayList();
    final ArrayList<Clientes> clientes = new ArrayList();
    
    public void crearCliente(String RUN, String nombre, String apellidos, String direccion,
            int telefono){
        clientes.add(new Clientes(RUN, nombre, apellidos, direccion, telefono));
    }
    
    public void crearCC(int cliente){
        cuentas.add(new CuentaCorriente(cliente));
    }
    
    public void crearCV(int cliente){
        cuentas.add(new CuentaVista(cliente));
    }
    
    public void crearFI(int cliente){
        cuentas.add(new FondoInversion(cliente));
    }
    
    public void revisionMensual(){
        for(int i = 0; i < cuentas.size(); i++){
            cuentas.get(i).revisionMensual();
        }
    }
    
    public int buscaraCliente(String Nombre, String Apellido){
        int posicion = -1;
        for(int i = 0; i < clientes.size(); i++){
            if ((Nombre == clientes.get(i).getNombre()) && (clientes.get(i).getApellidos() == Apellido)){
                posicion = i;
                break;
            }
        }
        return posicion;
    }
    
    public int buscaCuenta(int cliente){
        int posicion = -1;
        for(int i = 0; i < cuentas.size(); i++){
            if(cliente == cuentas.get(i).getCliente()){
                posicion = i;
                break;
            }
        }
        return posicion;
    }
}
