/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170417preparacionprueba;

/**
 *
 * @author andre
 */
public class Clientes {
    private final String RUN;
    private final String nombre;
    private final String apellidos;
    private String direccion;
    private int telefono;


    public Clientes(String RUN, String nombre, String apellidos, String direccion, int telefono) {
        this.RUN = RUN;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getRUN() {
        return RUN;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public int getTelefono() {
        return telefono;
    }
    
}
