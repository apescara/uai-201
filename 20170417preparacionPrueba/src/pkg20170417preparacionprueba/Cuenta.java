/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170417preparacionprueba;

/**
 *
 * @author andre
 */
public abstract class Cuenta {
    protected double saldo;
    protected int cliente;
    protected int sacarDinero;
    protected double interes;
    protected int comision;
    protected int puntos;

    public Cuenta(double saldo, int cliente, int sacarDinero, double interes, int comision, int puntos) {
        this.saldo = saldo;
        this.cliente= cliente;
        this.sacarDinero = sacarDinero;
        this.interes = interes;
        this.comision = comision;
        this.puntos = puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
    
    public void agregarSaldo(double agregar){
        this.saldo += agregar; 
        if(agregar > 1000){
            this.puntos += agregar%1000;
        }
    }
    
    public double retirar(double retiro){
        if(this.saldo - retiro > this.sacarDinero){
            this.saldo = this.saldo - retiro;
            return this.saldo;
        } else {
            return -1000000;
        }
    }
    
    public void revisionMensual(){
        this.saldo = this.saldo + (this.saldo*this.interes) - this.comision;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public void setComision(int comision) {
        this.comision = comision;
    }

    public double getSaldo() {
        return saldo;
    }

    public int getCliente() {
        return cliente;
    }

    public int getPuntos() {
        return puntos;
    }
    
    
}
