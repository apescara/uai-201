#include<stdio.h>
#include<string.h>
#include<time.h>

char nombres[32];
char apellidos[32];
int dia_nacimiento;
int mes_nacimiento;
int ano_nacimiento;
int Rut_sin_Digito;
int RUT[8];
int digito;

void agregar_datos() {
	printf("Ingrese su(s) nombre(s): \n");
	fflush(stdin);
	scanf(" %[^\n]", &nombres);
	printf("Ingrese sus apellidos: \n");
	fflush(stdin);
	scanf(" %[^\n]", &apellidos);
	printf("Ingrese sus dia de nacimiento: \n");
	fflush(stdin);
	scanf(" %d", &dia_nacimiento);
	fflush(stdin);
	printf("Ingrese sus mes de nacimiento: \n");
	fflush(stdin);
	scanf(" %d", &mes_nacimiento);
	fflush(stdin);
	printf("Ingrese sus ano de nacimiento: \n");
	fflush(stdin);
	scanf(" %d", &ano_nacimiento);
	fflush(stdin);
	printf("Ingrese su RUT sin digito ni puntos: \n");
	fflush(stdin);
	scanf(" %d", &Rut_sin_Digito);
	printf("Ingrese su digito verificador(si es K ingrese 10): \n");
	fflush(stdin);
	scanf(" %d", &digito);
}

void Calcular_edad() {
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	//printf("now: %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	int edad = 0;
	edad = (tm.tm_year + 1900) - ano_nacimiento;
	if ((mes_nacimiento > tm.tm_mon + 1) || (mes_nacimiento == tm.tm_mon + 1 && dia_nacimiento > tm.tm_mday)) {
		edad--;
	}
	printf("Su edad es: %d\n", edad);
}

void Verificar_digito() {
	int i = 0;
	while (Rut_sin_Digito > 0) {
		RUT[i] = Rut_sin_Digito % 10;
		// do something with digit
		Rut_sin_Digito /= 10;
		i++;
	}
	//printf("RUT: ");
	//for (int j = 0; j < 8; j++) {
	//	printf("%d", RUT[j]);
	//}
	int k = 2;
	int verificar = 0;
	for (int w = 0; w < 8; w++) {
		verificar = verificar + (RUT[w] * k);
		k++;
		if (k == 8) {
			k = 2;
		}
	}
	verificar = verificar % 11;
	if (11 - verificar == digito) {
		printf("El RUT es valido\n");
	}
	else {
		printf("El RUT es invalido\n");
	}
}

int main()
{
	agregar_datos();
	int finalizar = false;
	int opcion;
	printf("1.- Calcular su edad\n");
	printf("2.- Verificar digito verificador\n");
	printf("3.- Cerrar\n");
	while (finalizar == false) {
		opcion = -1;
		fflush(stdin);
		scanf("%d", &opcion);
		if (opcion == 3) finalizar = true;
		else if (opcion == 1) Calcular_edad();
		else if (opcion == 2) Verificar_digito();
		printf("1.- Calcular su edad\n");
		printf("2.- Verificar digito verificador\n");
		printf("3.- Cerrar\n");
	}
	return 0;
}