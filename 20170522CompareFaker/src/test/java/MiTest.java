/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.github.javafaker.Faker;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;

/**
 *
 * @author andre
 */
public class MiTest {
    
    Faker faker = new Faker();
    ArrayList<Delito> lista = new ArrayList<Delito>();
    
    public MiTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        faker = new Faker();
        lista = new ArrayList();
        for (int i = 0; i < 500; i++){
            lista.add(new Delito(faker.address().streetAddress(),faker.lorem().sentence(),
                    faker.number().numberBetween(1, 5), faker.options().option("Robo","Rayado","Mascota Perdida",
                    "Servicio Mal Estado","Ruidos Molesos"), faker.date().past(1000, TimeUnit.DAYS), 
                    "12:00"));
        }
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testTipo() {
        Collections.sort(lista,new TipoComparator());
    }
    
    @Test
    public void testFecha(){
        Collections.sort(lista, new FechaComparator());
    }
    
    @Test
    public void testIndex(){
        
    }
    
    
}
