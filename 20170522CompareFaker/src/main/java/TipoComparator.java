/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
/**
 *
 * @author andre
 */
public class TipoComparator implements Comparator{
    @Override
    public int compare(Object o1, Object o2){
        Delito d1 = (Delito)o1;
        Delito d2 = (Delito)o2;
        
        if(d1.getTipo().compareToIgnoreCase(d2.getTipo()) < 0){
            return -1;
        } else if (d1.getTipo().compareToIgnoreCase(d2.getTipo()) == 0){
            return 0;
        } else {
            return 1;
        }
    }
}
