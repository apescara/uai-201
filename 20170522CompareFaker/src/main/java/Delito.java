/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;

/**
 *
 * @author andre
 */
public class Delito {
    private final String ubicacion;
    private final String descripcion;
    private final int prioridad;
    private final String Tipo;
    private final Date fecha;
    private final String hora;

    public Delito(String ubicacion, String descripcion, int prioridad, String Tipo, Date fecha, String hora) {
        this.ubicacion = ubicacion;
        this.descripcion = descripcion;
        this.prioridad = prioridad;
        this.Tipo = Tipo;
        this.fecha = fecha;
        this.hora = hora;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public String getTipo() {
        return Tipo;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }
    
    
}
