/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170515rutwithexeption;

import java.security.InvalidParameterException;

/**
 *
 * @author andre
 */
public class RutInvalidoException extends InvalidParameterException {

    public RutInvalidoException() {
        super("RUT no valido");
    }

}
