/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170515rutwithexeption;

/**
 *
 * @author andre
 */
public class RutUtils {

    public void verificarRut(int rut, char dv) throws RutInvalidoException {
        int i = 0;
        int RUT[] = new int[8];

        while (rut > 0) {
            RUT[i] = rut % 10;
            rut /= 10;
            i++;
        }
        int a = 2;
        int verificar = 0;
        for (int w = 0; w < 8; w++) {
            verificar += RUT[w] * a;
            a++;
            if (a == 8) {
                a = 2;
            }
        }
        verificar = verificar % 11;
        int DVR = 11 - verificar;
        char DVI;
        if (DVR == 10) {
            DVI = 'K';
        } else {
            DVI = (char) ('0' + DVR);
        }
        if (dv != DVI) {
            throw new RutInvalidoException();
        }
    }
}
