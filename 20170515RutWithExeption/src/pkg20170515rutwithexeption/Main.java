/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170515rutwithexeption;

import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws RutInvalidoException {
        // TODO code application logic here
        try {
            Scanner scanner = new Scanner(System.in);
            int rut;
            char dv;
            System.out.println("Ingrese su Rut sin puntos ni digito verificador");
            rut = scanner.nextInt();
            System.out.println("Ingrese digito verificador (en caso de K usar mayusculas)");
            dv = scanner.next(".").charAt(0);
            Rut Rut = new Rut(rut, dv);
            System.out.println("Rut correcto");
        } catch (RutInvalidoException e) {
            System.out.println(e);
        }
    }
    
}
