/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170515rutwithexeption;

/**
 *
 * @author andre
 */
public class Rut {

    private int rut;
    private char dv;
    RutUtils Utils = new RutUtils();

    public Rut(int rut, char dv) throws RutInvalidoException {
        this.rut = rut;
        this.dv = dv;
        Utils.verificarRut(rut, dv);
    }
}
