/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170327tarea02;

/**
 *
 * @author andre
 */
public class Utils {
    static char calcularDV(int RUN){
        int i = 0;
        int RUT[] = new int [8];
        char DV = '0';
        while(RUN > 0){
           RUT[i] = RUN % 10;
           RUN /= 10;
           i++;
        }
        int a = 2;
        int verificar = 0;
        for(int w = 0; w < 8; w++){
            verificar += RUT[w]*a;
            a++;
            if(a == 8){
                a = 2;
            }
        }
        verificar = verificar % 11;
        int DVR = 11 - verificar; 
        if (DVR == 10){
            DV = 'k';
        } else {
            DV = (char) ('0' + DVR);
        }
        return DV;
    }
}
