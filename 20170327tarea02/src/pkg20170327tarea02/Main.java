/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170327tarea02;

import java.util.Scanner;
import java.util.Date;
/**
 *
 * @author andre
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scanner = new Scanner(System.in);       
        Date date = new Date();
        System.out.println("Ingrese su(s) nombre(s):");
        String nombre = scanner.nextLine();
        System.out.println("Ingrese apellidos:");
        String apellido = scanner.nextLine();
        Student persona1 = new StudentUAI(nombre, apellido);
        persona1.Hola();
        /* para que funcione lo siguiente hay que modificar persona 1 para 
        que sea Persona y no Student.
        System.out.println("Ingrese su dia de nacimiento:");
        persona1.setDiaNacimiento(scanner.nextInt());
        System.out.println("Ingrese su mes de nacimiento:");
        persona1.setMesNacimiento(scanner.nextInt());
        System.out.println("Ingrese su año de nacimiento");
        persona1.setAnoNacimiento(scanner.nextInt());
        System.out.println("Ingrese su RUN sin el digito verificador:");
        persona1.setRUN(scanner.nextInt());
        System.out.println("Ingrese su digito verificador (k en minusculas):");
        persona1.setDigitoVerificador(scanner.next(".").charAt(0));
        //persona1.nombres = "Andres";
        int edad = 0;      
        edad = (date.getYear() + 1900) - persona1.getAnoNacimiento();
        if((persona1.getMesNacimiento() > date.getMonth()) ||
                (persona1.getMesNacimiento() == date.getMonth() + 1 &&
                persona1.getDiaNacimiento() > date.getDay()) ){
            edad--;
        }
        System.out.println("La persona " + persona1.getNombres() + " " + persona1.getApellidos() + 
                " tiene " + edad + " años");
         System.out.println("RUN: " + persona1.getRUN() + "-" + persona1.getDigitoVerificador());
         char DV = persona1.getDigitoVerificador();
         char DVR = Utils.calcularDV(persona1.getRUN());
         if(DV == DVR){
             System.out.println("RUN correcto");
         }else{
             System.out.println("RUN incorrecto" + DVR + " " + DV);
         }
        */
    }
}
