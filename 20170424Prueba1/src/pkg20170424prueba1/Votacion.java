/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170424prueba1;
import java.util.ArrayList;
/**
 *
 * @author andre
 */
public class Votacion {
    private final int idVotacion; 
    private final int idGrupo;
    private final int idTransaccion;
    final ArrayList<Voto> votos = new ArrayList();

    public Votacion(int idVotacion, int idGrupo, int idTransaccion) {
        this.idVotacion = idVotacion;
        this.idGrupo = idGrupo;
        this.idTransaccion = idTransaccion;
    }
    
    
    
    public void agregarVoto(String usuario, String clave, int voto, int idVotacion,boolean votoCorrecto){
        votos.add(new Voto(usuario, clave,voto,idVotacion, votoCorrecto));
    }
    
    public boolean verificarVotacion(){
        int total = votos.size();
        int favor = 0;
        boolean decision = false;
        boolean verificar = true;
        for (int i = 0; i < total; i++){
            if(votos.get(i).isVotoCorrecto() == false){
                verificar = false;
            }
        }
        for (int i = 0; i < total; i++){
            favor += votos.get(i).getVoto();
        }
        if(favor > total/2 + 1 && verificar == true){
            decision = true;
        }
        return decision;
    }
}
