/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170424prueba1;
import java.util.Date;

/**
 *
 * @author andre
 */
public abstract class Usuario {
    protected String username;
    protected String clave;
    protected String nombre;
    protected String email;
    protected Date fechaNacimiento = null;
    protected boolean admin; // true es admin false no
    protected boolean trabajador;

    public Usuario(String username, String clave, String nombre, String email, Date fechaNacimiento, boolean admin, boolean trabajador) {
        this.username = username;
        this.clave = clave;
        this.nombre = nombre;
        this.email = email;
        this.fechaNacimiento = fechaNacimiento;
        this.admin = admin;
        this.trabajador = trabajador;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getUsername() {
        return username;
    }

    public boolean isTrabajador() {
        return trabajador;
    }

    public void setTrabajador(boolean trabajador) {
        this.trabajador = trabajador;
    }

    public String getClave() {
        return clave;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEmail() {
        return email;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
}
