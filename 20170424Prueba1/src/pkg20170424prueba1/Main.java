 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170424prueba1;
import java.text.ParseException;
import java.util.Scanner;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
/**
 *
 * @author andre
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {
        // TODO code application logic here
        Scanner scanner = new Scanner(System.in);
        ProgramaApoyo programa = new ProgramaApoyo();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        System.out.println("creacion grupo nombre: a, ciudad: b, pais: c");
        System.out.println("creacion cuanta grupo a, tipo: CC, monto inicial: 100");
        String nombreGrupo = "a";
        String ciudadGrupo = "b";
        String paisGrupo = "c";
        String tipoCuentaGrupo = "CC";
        int montoInicialGrupo = 100;
        String banco = "banco 1";
        int idCuenta = programa.crearCuenta(tipoCuentaGrupo, montoInicialGrupo, banco);
        int idGrupo = programa.crearGrupo(nombreGrupo, ciudadGrupo, paisGrupo, idCuenta);
        String usuarioAdmin = "admin";
        String claveAdmin = "admin";
        String nombreAdmin = "agente K";
        String emailAdmin = "k@k.k";
        String fechaNacimientoAdmin = "01/01/1990";
        System.out.println("creacion admin usuario: " + usuarioAdmin + ", clave: " + claveAdmin + 
                ", nombre: "+ nombreAdmin+ ", email: " + emailAdmin
                + ", fecha nacimiento: " + fechaNacimientoAdmin);
        programa.grupos.get(idGrupo).crearAdmin(usuarioAdmin, claveAdmin, nombreAdmin, emailAdmin, formatter.parse(fechaNacimientoAdmin));
        String usuarioT1 = "t1";
        String claveT1 = "t1";
        String nombreT1 = "m";
        String emailT1 = "m@m.m";
        String fechaNacimientoT1 = "01/02/1990";
        System.out.println("creacion admin usuario: " + usuarioT1 + ", clave: " + claveT1 + 
                ", nombre: "+ nombreT1+ ", email: " + emailT1
                + ", fecha nacimiento: " + fechaNacimientoT1);
        programa.grupos.get(idGrupo).crearTrabajador(usuarioT1, claveT1, nombreT1, emailT1,
                formatter.parse(fechaNacimientoT1));
        String usuarioT2 = "t2";
        String claveT2 = "t2";
        String nombreT2 = "l";
        String emailT2 = "l@l.l";
        String fechaNacimientoT2 = "01/02/1991";
        System.out.println("creacion admin usuario: " + usuarioT2 + ", clave: " + claveT2 + 
                ", nombre: "+ nombreT2+ ", email: " + emailT2
                + ", fecha nacimiento: " + fechaNacimientoT2);
        programa.grupos.get(idGrupo).crearTrabajador(usuarioT2, claveT2, nombreT2, emailT2, formatter.parse(fechaNacimientoT2));
        System.out.println("admin aprueba deposito de m a la cuanta del grupo a por un valor de 50? 1 si, 0 no");
        int decision = scanner.nextInt();
        System.out.println("Ingrese su clave");
        String verificacionPass = scanner.nextLine();
        boolean claveCorrecta = false;
        if(verificacionPass == claveAdmin){
            claveCorrecta = true;
        }
        int idTrans = programa.trans.size() + 1;
        boolean dAprobado = programa.depositoCuenta(idCuenta, decision, 50, claveCorrecta, idTrans);
        if(dAprobado == true){
            System.out.println("se deposito con exito");
        } else {
            System.out.println("no se pudo realizar el deposito");
        }
        System.out.println("el trabajador m quiere retirar 155 de la cuenta del grupo a");
        System.out.println("voto tabajador m: (1 aprueba, 0 desaprueba)");
        int votoTm = scanner.nextInt();
        System.out.println("trabajador m: ingrese su clave");
        String veriClaveTm = scanner.nextLine();
        System.out.println("voto tabajador 2: (1 aprueba, 0 desaprueba)");
        int votoT1 = scanner.nextInt();
        System.out.println("trabajador 2: ingrese su clave");
        String veriClaveT2 = scanner.nextLine();
        idTrans = programa.trans.size() + 1;
        int idVotacion = programa.votaciones.size() + 1;
        programa.crearVotacion(idVotacion, idGrupo, idTrans);
        int idT = programa.grupos.get(idGrupo).usuarios.size() + 1;
        for(int i = 0; i < programa.grupos.get(idGrupo).usuarios.size();i++){
            if(usuarioT1 == programa.grupos.get(idGrupo).usuarios.get(i).getUsername()){
                idT = i;
            }
        }
        boolean vTmCorrecto = false;
        if(programa.grupos.get(idGrupo).usuarios.get(idT).getClave() == veriClaveTm){
            vTmCorrecto = false;
        }
        idT = programa.grupos.get(idGrupo).usuarios.size() + 1;
        for(int i = 0; i < programa.grupos.get(idGrupo).usuarios.size();i++){
            if(usuarioT2 == programa.grupos.get(idGrupo).usuarios.get(i).getUsername()){
                idT = i;
            }
        }
        boolean vT1Correcto = false;
        if(programa.grupos.get(idGrupo).usuarios.get(idT).getClave() == veriClaveTm){
            vT1Correcto = false;
        }
        programa.agregarVoto(idVotacion, usuarioT1, veriClaveTm, votoTm, vTmCorrecto);
        programa.agregarVoto(idVotacion, usuarioT2, veriClaveT2, votoT1, vT1Correcto);
        boolean decisionVotacion = programa.votaciones.get(idVotacion - 1).verificarVotacion();
        boolean rAprobado = programa.retiroCuenta(idCuenta, decisionVotacion, 155, idGrupo, idTrans);
        if(rAprobado == true){
            System.out.println("se retiro con exito");
        } else {
            System.out.println("no se pudo realizar el retiro");
        }
        System.out.println("el trabajador m quiere retirar 50 de la cuenta del grupo a");
        System.out.println("voto tabajador m: (1 aprueba, 0 desaprueba)");
        votoTm = scanner.nextInt();
        System.out.println("trabajador m: ingrese su clave");
        veriClaveTm = scanner.nextLine();
        System.out.println("voto tabajador 2: (1 aprueba, 0 desaprueba)");
        votoT1 = scanner.nextInt();
        System.out.println("trabajador 2: ingrese su clave");
        veriClaveT2 = scanner.nextLine();
        idTrans = programa.trans.size() + 1;
        idVotacion = programa.votaciones.size() + 1;
        programa.crearVotacion(idVotacion, idGrupo, idTrans);
        idT = programa.grupos.get(idGrupo).usuarios.size() + 1;
        for(int i = 0; i < programa.grupos.get(idGrupo).usuarios.size();i++){
            if(usuarioT1 == programa.grupos.get(idGrupo).usuarios.get(i).getUsername()){
                idT = i;
            }
        }
        vTmCorrecto = false;
        if(programa.grupos.get(idGrupo).usuarios.get(idT).getClave() == veriClaveTm){
            vTmCorrecto = false;
        }
        idT = programa.grupos.get(idGrupo).usuarios.size() + 1;
        for(int i = 0; i < programa.grupos.get(idGrupo).usuarios.size();i++){
            if(usuarioT2 == programa.grupos.get(idGrupo).usuarios.get(i).getUsername()){
                idT = i;
            }
        }
        vT1Correcto = false;
        if(programa.grupos.get(idGrupo).usuarios.get(idT).getClave() == veriClaveTm){
            vT1Correcto = false;
        }
        programa.agregarVoto(idVotacion, usuarioT1, veriClaveTm, votoTm, vTmCorrecto);
        programa.agregarVoto(idVotacion, usuarioT2, veriClaveT2, votoT1, vT1Correcto);
        decisionVotacion = programa.votaciones.get(idVotacion - 1).verificarVotacion();
        rAprobado = programa.retiroCuenta(idCuenta, decisionVotacion, 50, idGrupo, idTrans);
        if(rAprobado == true){
            System.out.println("se retiro con exito");
        } else {
            System.out.println("no se pudo realizar el retiro");
        }
    }
}
