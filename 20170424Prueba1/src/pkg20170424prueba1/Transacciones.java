/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170424prueba1;

/**
 *
 * @author andre
 */
public class Transacciones {
    private final int idTransaccion;
    private final int idCuenta;
    private final String tipo;
    private final boolean aprobado;
    private final int monto;

    public Transacciones(int idCuenta, String tipo, boolean aprobado, int monto, int idTrans) {
        this.idCuenta = idCuenta;
        this.tipo = tipo;
        this.aprobado = aprobado;
        this.monto = monto;
        this.idTransaccion = idTrans;
    }

    public int getMonto() {
        return monto;
    }

    public int getIdCuenta() {
        return idCuenta;
    }

    public String getTipo() {
        return tipo;
    }

    public boolean isAprobado() {
        return aprobado;
    }
    
}
