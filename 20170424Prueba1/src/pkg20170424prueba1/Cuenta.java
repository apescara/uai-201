/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170424prueba1;

/**
 *
 * @author andre
 */
public class Cuenta {
    private final String banco;
    private final String tipo;
    private int monto;
    private int idGrupo;

    public Cuenta(String tipo, int monto, String Banco) {
        this.tipo = tipo;
        this.monto = monto;
        this.banco = Banco;
    }

    public String getBanco() {
        return banco;
    }

    public String getTipo() {
        return tipo;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }
    
}
