/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170424prueba1;
import java.util.ArrayList;
import java.util.Date;
/**
 *
 * @author andre
 */
public class Grupo {
    final ArrayList<Usuario> usuarios = new ArrayList();
    private final String nombre;
    private final String ciudad;
    private final String pais;
    private int cuenta;

    public Grupo(String nombre, String ciudad, String pais, int cuenta) {
        this.nombre = nombre;
        this.ciudad = ciudad;
        this.pais = pais;
        this.cuenta = cuenta;
    }
    
    
    public boolean crearAdmin(String username, String clave, String nombre, String email, Date fechaNacimiento){
        boolean hayAdmin = false;
        for (int i = 0; i < usuarios.size(); i++){
            if(usuarios.get(i).isAdmin() == true){
                hayAdmin = true;
            }
        }
        if(hayAdmin == false){
            usuarios.add(new Admin(username, clave, nombre, email, fechaNacimiento));
        }
        return hayAdmin;
    }
    
    public void crearTrabajador(String username, String clave, String nombre, String email, Date fechaNacimiento){
        usuarios.add(new Trabajador(username, clave, nombre, email, fechaNacimiento));
    }
    
    public void crearIntegrante(String username, String clave, String nombre, String email, Date fechaNacimiento){
        usuarios.add(new Integrante(username, clave, nombre, email, fechaNacimiento));
    }
    
    public void promoverIntegranteTrabajador(String usuario){
        int id = usuarios.size() + 1; 
        for(int i = 0; i < usuarios.size(); i++){
            if(usuarios.get(i).getUsername() == usuario){
                id = i;
            }
        }
        if(id != usuarios.size() + 1){
            usuarios.add(new Trabajador(usuarios.get(id).getUsername(), usuarios.get(id).getClave(), 
                    usuarios.get(id).getNombre(),usuarios.get(id).getEmail(), usuarios.get(id).getFechaNacimiento()));
        }
    }
    
    public void promoverIntegranteAdmin(String usuario){
       int id = usuarios.size() + 1; 
        for(int i = 0; i < usuarios.size(); i++){
            if(usuarios.get(i).getUsername() == usuario){
                id = i;
            }
        }
        if(id != usuarios.size() + 1){
            usuarios.add(new Admin(usuarios.get(id).getUsername(), usuarios.get(id).getClave(), 
                    usuarios.get(id).getNombre(),usuarios.get(id).getEmail(), usuarios.get(id).getFechaNacimiento()));
        } 
    }

    public String getNombre() {
        return nombre;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCuenta(int cuenta) {
        this.cuenta = cuenta;
    }

    public String getPais() {
        return pais;
    }

    public int getCuenta() {
        return cuenta;
    }
    
    
}
