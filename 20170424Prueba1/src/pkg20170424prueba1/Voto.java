/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170424prueba1;

/**
 *
 * @author andre
 */
public class Voto {
    private final String usuario;
    private final String clave;
    private final int voto;
    private final int idVotacion;
    private final boolean votoCorrecto;

    public Voto(String usuario, String clave, int voto, int idVotacion, boolean correcto) {
        this.usuario = usuario;
        this.clave = clave;
        this.voto = voto;
        this.idVotacion = idVotacion;
        this.votoCorrecto = correcto;
    }

    public boolean isVotoCorrecto() {
        return votoCorrecto;
    }

    
    public String getUsuario() {
        return usuario;
    }

    public String getClave() {
        return clave;
    }

    public int getVoto() {
        return voto;
    }

    public int getIdVotacion() {
        return idVotacion;
    }
    
    
    
}
