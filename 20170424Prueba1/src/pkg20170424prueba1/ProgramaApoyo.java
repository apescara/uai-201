/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20170424prueba1;
import java.util.ArrayList;
/**
 *
 * @author andre
 */
public class ProgramaApoyo {
    final ArrayList<Grupo> grupos = new ArrayList();
    private final ArrayList<Cuenta> cuentas = new ArrayList ();
    final ArrayList<Transacciones> trans = new ArrayList();
    final ArrayList<Votacion> votaciones = new ArrayList();
    
    public int crearGrupo(String nombre, String ciudad, String pais, int cuenta){
        grupos.add(new Grupo(nombre, ciudad, pais, cuenta));
        int idGrupo = grupos.size() - 1;
        cuentas.get(cuenta).setIdGrupo(idGrupo);
        return idGrupo;
    }
    
    public int crearCuenta(String tipo, int monto, String Banco){
        cuentas.add(new Cuenta(tipo, monto, Banco));
        return cuentas.size() - 1;
    }
    
    public int cambiarCuenta(int idCuenta, int idGrupo){
        grupos.get(idGrupo).setCuenta(idCuenta);
        return idCuenta;
    }
    
    public String getTipoCuenta(int idCuenta) {
        String tipo = cuentas.get(idCuenta).getTipo();
        return tipo;
    }

    public int getMontoCuenta(int idCuenta) {
        int monto = cuentas.get(idCuenta).getMonto();
        return monto;
    }

    public void setMontoCuenta(int idCuenta, int monto) {
        cuentas.get(idCuenta).setMonto(monto);
    }

    public int getIdGrupoCuenta(int idCuenta) {
        int idGrupo = cuentas.get(idCuenta).getIdGrupo();
        return idGrupo;
    }
    
    public boolean depositoCuenta(int idCuenta, int decisionAdmin, int aDepositar, boolean passCorrecta, int idTrans){
        int monto  = cuentas.get(idCuenta).getMonto();
        boolean aprobado = false;
        if(decisionAdmin == 1 && passCorrecta == true){
            cuentas.get(idCuenta).setMonto(monto + aDepositar);
            aprobado = true;
            trans.add(new Transacciones(idCuenta, "deposito", true, aDepositar, idTrans));
        }
        else{
            trans.add(new Transacciones(idCuenta, "deposito", false, aDepositar, idTrans));
        }
        return aprobado;
    }
    
    public boolean retiroCuenta(int idCuenta, boolean votos, int retiro, int idGrupo, int idTrans){
        boolean aprobado = false;
        int monto  = cuentas.get(idCuenta).getMonto();
        int numeroTrabajadores = grupos.get(idGrupo).usuarios.size() - 1;
        if(monto >= retiro){
            if(votos == true){
                cuentas.get(idCuenta).setMonto(monto - retiro);
                aprobado = true;
                trans.add(new Transacciones(idCuenta, "retiro", true, retiro, idTrans));
            }
            else {
                trans.add(new Transacciones(idCuenta, "retiro", false, retiro, idTrans));
            }
        } else {
            trans.add(new Transacciones(idCuenta, "retiro", false, retiro, idTrans));
        }
        return aprobado;
    }
    
    public void crearVotacion(int idVotacion, int idGrupo, int idTransaccion){
        votaciones.add(new Votacion(idVotacion,idGrupo,idTransaccion));
    }
    
    public void agregarVoto(int idVotacion, String usuario, String clave, int voto, boolean votoCorrecto){
        votaciones.get(idVotacion).agregarVoto(usuario, clave, voto, idVotacion, votoCorrecto);
    }
}
